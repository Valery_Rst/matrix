package net.martix;

public class MatrixExFirst {public static void main(String[] args) {
    int firstValue = 4;
    int secondValue = 4;
    int[][] matrix = new int[firstValue][secondValue];
    matrix[0][0] = 4;
    matrix[0][1] = -1;
    matrix[0][2] = 5;
    matrix[0][3] = 8;
    matrix[1][0] = 4;
    matrix[1][1] = 8;
    matrix[1][2] = -3;
    matrix[1][3] = 9;
    matrix[2][0] = 5;
    matrix[2][1] = 2;
    matrix[2][2] = 9;
    matrix[2][3] = 4;
    matrix[3][0] = 4;
    matrix[3][1] = 8;
    matrix[3][2] = 7;
    matrix[3][3] = 5;

    int sum = 0;
    for (int i = firstValue / 2; i < firstValue; i++) {
        for (int j = 0; j < secondValue / 2; j++) {
            sum += matrix[i][j];
        }
    }

    for (int i = 0; i < firstValue/2; i++) {
        for (int j = secondValue / 2; j < secondValue; j++) {
            sum += matrix[i][j];
        }
    }
    System.out.println("Сумма = " + sum);
}
}
