package net.martix;

public class MatrixExThird {
    public static void main(String[] args) {
        int firstValue = 4;
        int secondValue = 4;
        int[][] matrix = new int[firstValue][secondValue];
        matrix[0][0] = 7;
        matrix[0][1] = -2;
        matrix[0][2] = 3;
        matrix[0][3] = 4;
        matrix[1][0] = 4;
        matrix[1][1] = 7;
        matrix[1][2] = 6;
        matrix[1][3] = 2;
        matrix[2][0] = 6;
        matrix[2][1] = 2;
        matrix[2][2] = 9;
        matrix[2][3] = 1;
        matrix[3][0] = 1;
        matrix[3][1] = 6;
        matrix[3][2] = 7;
        matrix[3][3] = 2;

        int sum = 0;
        for (int i = 0; i < firstValue; i++) {
            for (int j = 0; j < secondValue; j++) {
                if(i==j) {
                    sum += matrix[i][j];
                }
            }
        }
        System.out.println("Сумма = " + sum);
    }
}
