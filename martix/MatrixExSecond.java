package net.martix;

public class MatrixExSecond {
    public static void main(String[] args) {
        int firstValue = 4;
        int secondValue = 4;
        int[][] matrix = new int[firstValue][secondValue];
        matrix[0][0] = 7;
        matrix[0][1] = -2;
        matrix[0][2] = 1;
        matrix[0][3] = 4;
        matrix[1][0] = 1;
        matrix[1][1] = 7;
        matrix[1][2] = 9;
        matrix[1][3] = 1;
        matrix[2][0] = 5;
        matrix[2][1] = 0;
        matrix[2][2] = 9;
        matrix[2][3] = 3;
        matrix[3][0] = 5;
        matrix[3][1] = 6;
        matrix[3][2] = 7;
        matrix[3][3] = 4;

        int sum = 0;
        for (int i = 0; i < firstValue; i++) {
            for (int j = 0; j < secondValue; j++) {
                if(j > i) {
                    sum += matrix[i][j];
                }
            }
        }
        System.out.println("Сумма = " + sum);
    }
}
